"use strict";

require("dotenv").config();

import express from "express";
import users from "../data/users";
import { v4 as uuidv4 } from "uuid";
import verifyToken from "../src/middleware/jwtVerify";
import { readCats, writeCats } from "./util/jsonHandler";

const router = express.Router();
let jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const saltRounds = 10;

//testing route - hello world
router.get("/", (req, res) => {
  return res.status(200).send("Hello World");
});

// gets the full list of cats
router.get("/cats", verifyToken, (req, res) => {
  readCats().then((cats) => {
    return res.status(200).send(cats);
  });
});

// adds a new cat to the database
router.post("/cats", (req, res) => {
  let newCat = {
    id: uuidv4(),
    name: req.body.name,
    breed: req.body.breed,
  };
  readCats().then((catsArray) => {
    catsArray.push(newCat);
    writeCats(catsArray);
    return res.status(201).send(newCat);
  });
});

// registers a user
// hash the password
// create a token
router.post("/register", (req, res) => {
  let newUser = {
    email: req.body.email,
    password: req.body.password,
  };

  bcrypt.hash(newUser.password, saltRounds, function (err, hash) {
    // Store hash in your password DB.
    newUser.password = hash;

    let token = jwt.sign(newUser, `${process.env.privateKey}`);
    users.push(newUser);
    console.log(newUser);
    return res.status(201).send(token);
  });
});

//catch all route if URL is not listed here
router.get("*", (req, res, next) => {
  let err = new Error(`typed wrong URL`);
  next(err);
});

export default router;
